const gulp = require('gulp');
const $ = require('gulp-load-plugins')();
const { SpecReporter } = require('jasmine-spec-reporter');
const config = require('../config');

gulp.task('integration-test', () =>
  gulp.src(config.paths.integration)
    // Running tests with jasmine, output each test case
    .pipe($.jasmine({
      verbose: true,
      reporter: new SpecReporter(),
    })));
