const { Logger } = require('log4js/lib/logger');

// Setup jasmine-promises
// see https://github.com/matthewjh/jasmine-promises/issues/8
global.jasmineRequire = {
  interface: () => {},
};
require('jasmine-promises');

// disable logging in tests
// eslint-disable-next-line no-underscore-dangle
Logger.prototype._log = () => ({});

// Setup console.inspect()
const util = require('util');

console.inspect = (title, ...args) => {
  console.log(title, util.inspect(args, false, 7, true));
};

console.log('Test setup completed');

// Setup chai and chai-as-promised
global.chai = require('chai');
global.chai.use(require('chai-as-promised'));
global.chai.use(require('chai-subset'));

global.chai.should();
