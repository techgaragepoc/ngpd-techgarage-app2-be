const envs = require('envs');
const fs = require('fs');
const R = require('ramda');

const toArray = value => value.split(',').map(R.pipe(R.trim, R.toLower));
// const toString = (values = []) => values.join(',');

const config = {
  app_name: () => envs('APP_NAME', 'app-name-change-me'),
  // Application environment
  app_env: () => envs('APP_ENV', 'development'),

  // Frontend URL for CORS
  frontend_url: () => envs('FRONTEND_URL', 'http://localhost:8400'),

  // Port for the app
  port: () => process.env.port || envs('PORT', 5001),

  // The path on the frontend to be redirected to after login
  frontend_path_after_login_success: () => envs('FRONTEND_PATH_AFTER_LOGIN', '/redirect/auth/success'),
  frontend_path_after_login_failure: () => envs('FRONTEND_PATH_AFTER_FAILURE', '/redirect/auth/failure'),

  // How long to wait for a request
  request_timeout: () => parseInt(envs('REQUEST_TIMEOUT', 3000), 10),

  // How many redirects are taken
  request_follow: () => parseInt(envs('REQUEST_FOLLOW', 0), 10),

  // MongoDB URI
  mongodb_uri: () => envs('MONGODB_URI'),

  // Mercer SSO service
  sso_entry_point: () => envs('SSO_ENTRY_POINT', 'https://msso1-dev.mercer.com/adfs/ls/'),
  sso_private_cert: () => envs('SSO_PRIVATE_CERT', fs.readFileSync('./key.pem', 'utf-8')),
  // Backend URL for callback from Mercer SSO service
  backend_url: () => envs('BACKEND_URL', 'http://localhost:5001'),
  // The key to use to sign & verify cookie values
  session_secret: () => envs('SESSION_SECRET', 'supersecret'),
  session_cookie_name: () => envs('SESSION_COOKIE_NAME', 'app-name-change-me.sid'),
  session_collection: () => envs('SESSION_COLLECTION', 'sessions'),

  securedHeaders: () => toArray(envs('SECURED_HEADERS', 'APIKEY')),
  securedCookies: () => toArray(envs('SECURED_COOKIES', config.session_cookie_name())),

  // Custom headers and values
  customHeaders: () => toArray(envs('CUSTOM_HEADERS', 'JWT')),
  apiKey: () => envs('API_KEY', 'M7wEH4Bmi6AJnq9UfBydfhphGuzj8bK8'),

  // EXAMPLES
  // Pass Through Microservices
  api_url_microservice_pass_through: () => envs('API_MICROSERVICE_PASS_THROUGH', 'http://localhost:3000'),

  // Microservices to aggregate
  api_url_microservice_to_aggregate: () => envs('API_MICROSERVICE_TO_AGGREGATE', 'http://localhost:3000'),

};

// module.exports = { config, toArray, toString };
module.exports = config;
