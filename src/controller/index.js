const express = require('express');
const UserController = require('./user.ctrl');
const CarController = require('./car.ctrl');
const ProxyExampleController = require('./proxy-ex.ctrl');

const router = express.Router();

const userController = new UserController();
router.route('/user')
  .get(userController.getUserIdByEmail.bind(userController))
  .post(userController.createUser.bind(userController));

const carController = new CarController();
router.route('/car')
  .post(carController.createCar.bind(carController))
  .put(carController.updateCar.bind(carController));
router.route('/car/:id')
  .get(carController.readCar.bind(carController))
  .delete(carController.deleteCar.bind(carController));

const prExController = new ProxyExampleController();
router.route('/example2/*')
  .get(prExController.getExampleData.bind(prExController));

module.exports = router;
