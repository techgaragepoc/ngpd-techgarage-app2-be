const {
  RestController
} = require('ngpd-merceros-common-be/controller');

const {
  CarService
} = require('../service');

class CarRestController extends RestController {
  constructor() {
    super();
    this.carService = new CarService();
  }

  createCar(req, res, next) {
    const { body } = req;
    return this.carService.createCar(body)
      .then(this.responseWithResult(res))
      .catch(next);
  }

  readCar(req, res, next) {
    const { id } = req.params;
    return this.carService.getCarById(id)
      .then(this.responseWithResult(res))
      .catch(next);
  }

  updateCar(req, res, next) {
    const { body } = req;
    return this.carService.updateCar(body)
      .then(this.responseWithResult(res))
      .catch(next);
  }

  deleteCar(req, res, next) {
    const { id } = req.params;
    return this.carService.deleteCarById(id)
      .then(this.responseWithResult(res))
      .catch(next);
  }

}

module.exports = CarRestController;
