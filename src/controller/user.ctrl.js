const {
  RestController
} = require('ngpd-merceros-common-be/controller');

const {
  UserService
} = require('../service');

class UserRestController extends RestController {
  constructor() {
    super();
    this.userService = new UserService();
  }

  getUserIdByEmail(req, res, next) {
    return this.userService.getUserIdByEmail(req.query.email)
      .then(this.responseWithResult(res))
      .catch(next);
  }

  createUser(req, res, next) {
    const { body } = req;
    return this.userService.createUser(body)
      .then(this.responseWithResult(res))
      .catch(next);
  }
}

module.exports = UserRestController;
