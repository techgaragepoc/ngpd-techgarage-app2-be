const { UserModel } = require('ngpd-merceros-common-be/model');

class UserService {

  constructor() {
    this.Entity = UserModel;
  }

  getUserIdByEmail(email) {
    return this.Entity.findOne({ email }, 'id');
  }

  createUser(user) {
    return this.Entity.create(user);
  }

}

module.exports = UserService;
