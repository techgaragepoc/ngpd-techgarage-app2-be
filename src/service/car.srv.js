const { CarModel } = require('../model');

class CarService {

  constructor() {
    this.Entity = CarModel;
  }

  getCarById(_id) {
    return this.Entity.findOne({ _id });
  }

  createCar(car) {
    return this.Entity.create(car);
  }

  updateCar(car) {
    return this.Entity.update(car);
  }

  deleteCarById(_id) {
    return this.Entity.deleteOne({ _id });
  }

}

module.exports = CarService;
